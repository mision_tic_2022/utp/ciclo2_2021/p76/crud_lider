package com.utp.p76.controlador;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseController {

    public static Connection conectar_bd() throws SQLException {
        return DriverManager.getConnection("jdbc:sqlite:ProyectosConstruccion.db");
    }

}
