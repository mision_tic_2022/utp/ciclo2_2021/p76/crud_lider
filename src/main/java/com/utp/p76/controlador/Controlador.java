package com.utp.p76.controlador;

import com.utp.p76.modelo.dao.DaoLider;
import com.utp.p76.modelo.vo.Lider;

public class Controlador {
    // Atributos
    private DaoLider daoLider;

    // Constructor
    public Controlador() {
        this.daoLider = new DaoLider();
    }

    // Acciones

    public Lider buscar_lider(String documento_identidad) {
        return daoLider.buscar_lider(documento_identidad);
    }

    public void insertar_lider(Lider objLider) {
        daoLider.insertar_lider(objLider);
    }

    public Lider construir_lider(int id, String nombre, String primer_apelido, String segundo_apellido, int salario,
            String ciudad_residencia, String cargo, int clasificacion, String documento_identidad,
            String fecha_nacimiento) {
        return new Lider(id, nombre, primer_apelido, segundo_apellido, salario, ciudad_residencia, cargo, clasificacion,
                documento_identidad, fecha_nacimiento);
    }

    public Lider contruir_lider() {
        return new Lider();
    }

    public void actualizar_lider(Lider lider, int id) {
        daoLider.actualizar_lider(lider, id);
    }
}