package com.utp.p76.vista;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import com.utp.p76.controlador.Controlador;
import com.utp.p76.modelo.vo.Lider;

import java.awt.*;
import java.awt.event.*;

public class VistaFormulario extends JFrame{
    /*************
     * Atributos
     ************/
    //Label -> Mostrar texto
    private JLabel lblDocumento;
    private JLabel lblNombre;
    private JLabel lblPrimerApellido;
    private JLabel lblSegundoApellido;
    private JLabel lblSalario;
    //Campos de texto
    private JTextField txtDocumento;
    private JTextField txtNombre;
    private JTextField txtPrimerApellido;
    private JTextField txtSegundoApellido;
    private JTextField txtSalario;
    //Botones
    private JButton btnBuscar;
    private JButton btnCrear;
    //Controlador
    private Controlador objControlador;

    //Cnstructor
    public VistaFormulario(){
        this.objControlador = new Controlador();
        //Titulo de la ventana
        this.setTitle("Formulario Lideres");
        //Ubicación y tamaño
        this.setBounds(0, 0, 450, 250);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        //Construir objeto layout
        GridLayout gLayout = new GridLayout(5,2, 5, 5);
        //Contenedor de elementos
        Container objContainer = new Container();
        //Indicar el layout al objContainer
        objContainer.setLayout(gLayout);

        BorderLayout bLayout = new BorderLayout(5, 5);
        Container contentPane = this.getContentPane();
        contentPane.setLayout(bLayout);

        //inicializar los elementos gráficos y añadirlos al esquema

        this.lblNombre = new JLabel("Nombre: ");
        objContainer.add(this.lblNombre);

        this.txtNombre = new JTextField();
        objContainer.add(this.txtNombre);

        this.lblPrimerApellido = new JLabel("Primer apellido: ");
        objContainer.add(this.lblPrimerApellido);

        this.txtPrimerApellido = new JTextField();
        objContainer.add(this.txtPrimerApellido);

        this.lblSegundoApellido = new JLabel("Segundo apelido: ");
        objContainer.add(this.lblSegundoApellido);

        this.txtSegundoApellido = new JTextField();
        objContainer.add(this.txtSegundoApellido);

        this.lblSalario = new JLabel("Salario: ");
        objContainer.add(this.lblSalario);

        this.txtSalario = new JTextField();
        objContainer.add(this.txtSalario);

        GridLayout northGridLayout = new GridLayout(1, 3, 5, 5);
        Container northContainer = new Container();
        northContainer.setLayout(northGridLayout);

        this.lblDocumento = new JLabel("Documento identidad: ");
        northContainer.add(this.lblDocumento);

        this.txtDocumento = new JTextField();
        northContainer.add(this.txtDocumento);

        this.btnBuscar = new JButton("Buscar");
        northContainer.add(this.btnBuscar);

        objContainer.add(new JLabel());
        this.btnCrear = new JButton("Crear lider");
        objContainer.add(this.btnCrear);

        //Manejador de eventos
        this.btnBuscar.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                String documento = txtDocumento.getText();
                Lider objLider = objControlador.buscar_lider(documento);
                //Poner la información en los campos de texto
                txtNombre.setText( objLider.getNombre() );
                txtPrimerApellido.setText( objLider.getPrimer_apellido() );
                txtSegundoApellido.setText( objLider.getSegundo_apellido() );
                txtSalario.setText( ""+objLider.getSalario() );
            }
        });

        this.add(objContainer, BorderLayout.CENTER);
        this.add(northContainer, BorderLayout.NORTH);
    }
    
}

