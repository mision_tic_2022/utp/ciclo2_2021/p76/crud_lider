package com.utp.p76.vista;
import java.util.Scanner;

import com.utp.p76.controlador.Controlador;
import com.utp.p76.modelo.vo.Lider;

public class Vista {
    //Atributo
    private Controlador controlador;

    //Constructor
    public Vista(){
        this.controlador = new Controlador();
    }

    public void buscar_lider(){
        try (Scanner entrada = new Scanner(System.in)){
            
            System.out.println("Por favor ingrese el documento del lider a buscar: ");
            Lider objLider = this.controlador.buscar_lider( entrada.next() );
            this.mostrar_lider(objLider);

        } catch (Exception e) {
            //TODO: handle exception
            System.out.println(e);
        }
        this.crear_menu();
    }

    public void mostrar_lider(Lider lider) {
        System.out.println("------------LIDER-----------");
        System.out.println("");
        System.out.println("Id: " + lider.getId());
        System.out.println("Nombre: " + lider.getNombre());
        System.out.println("Primer apellido: " + lider.getPrimer_apellido());
        System.out.println("Segundo apellido: " + lider.getSegundo_apellido());
        System.out.println("Salario: $" + lider.getSalario());
        System.out.println("Ciudad residencia: " + lider.getCiudad_residencia());
        System.out.println("Cargo: " + lider.getCargo());
        System.out.println("Clasificación: " + lider.getClasificacion());
        System.out.println("Documento de identidad: " + lider.getDocumento_identidad());
        System.out.println("Fecha nacimiento: " + lider.getFecha_nacimiento());
        System.out.println("");
        System.out.println("-------------------------------------------");

    }

    public Lider crear_formulario() {
        Lider lider = this.controlador.contruir_lider();
        try (Scanner entrada = new Scanner(System.in)) {

            System.out.println("Por favor ingrese el id: ");
            int id = entrada.nextInt();

            System.out.println("por favor ingrese el nombre: ");
            String nombre = entrada.next();

            System.out.println("Por favor ingrese el primer apellido: ");
            String pApellido = entrada.next();

            System.out.println("Por favor ingrese el segundo apellido: ");
            String sApellido = entrada.next();

            System.out.println("Por favor ingrese el salario: ");
            int salario = entrada.nextInt();

            System.out.println("Por favor ingrese la ciudad de residencia: ");
            String ciudad = entrada.next();

            System.out.println("Por favor ingrese el cargo: ");
            String cargo = entrada.next();

            System.out.println("Por favor ingrese la clasificación: ");
            int clasificacion = entrada.nextInt();

            System.out.println("Por favor ingrese el documento de identidad: ");
            String documento = entrada.next();

            System.out.println("Por favor ingrese la fecha de nacimiento (yyyy-mm-dd) :");
            String fecha_nacimiento = entrada.next();
            
            lider = this.controlador.construir_lider(id, nombre, pApellido, sApellido, salario, ciudad, cargo, clasificacion, documento, fecha_nacimiento);

        } catch (Exception e) {
            // TODO: handle exception
            System.err.println(e);
        }

        return lider;
    }

    public void crear_lider() {
        Lider objLider = this.crear_formulario();
        this.controlador.insertar_lider(objLider);
        this.crear_menu();
    }


    public void actualizar_lider(){
        try (Scanner entrada = new Scanner(System.in)){
            
            System.out.println("Por favor ingrese el documento del lider a actualizar");
            String documento = entrada.next();
            Lider lider = this.controlador.buscar_lider(documento);
            if(lider.getNombre() == null){
                System.out.println("El lider con el documento ingresado no existe");
            }else{
                this.mostrar_lider(lider);
                System.out.println("");
                System.out.println("--------ACTUALIZAR INFORMACIÓN---------");
                System.out.println("Formulario de actualización: ");
                Lider objLider = this.crear_formulario();
                this.controlador.actualizar_lider(objLider, lider.getId());
            }

        } catch (Exception e) {
            //TODO: handle exception
            System.err.println(e);
        }
        this.crear_menu();
    }


    public void crear_menu(){
        System.out.println("-----------CRUD LIDER-----------");
        System.out.println("1 -->>  Registrar un lider");
        System.out.println("2 -->>  Buscar un lider");
        System.out.println("3 -->>  Actualizar lider");
        System.out.println("0 -->>  Salir");

        try (Scanner entrada = new Scanner(System.in)){

            int opcion = entrada.nextInt();

            switch(opcion){
                case 1:
                    this.crear_lider();
                    break;                    
                case 2:
                    this.buscar_lider();
                    break;
                case 3:
                    this.actualizar_lider();
                    break;
                default:
                    System.out.println("Opción incorrecta");
                    this.crear_menu();
                    break;
            }
            
        }catch (Exception e) {
            //TODO: handle exception
            System.out.println(e);
            System.out.println(e.getMessage());
        }
    }

}

