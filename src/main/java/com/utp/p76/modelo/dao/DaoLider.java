package com.utp.p76.modelo.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.utp.p76.controlador.DatabaseController;
import com.utp.p76.modelo.vo.Lider;

public class DaoLider {

    public Lider buscar_lider(String documento_identidad) {
        Lider objLider = new Lider();
        try {
            /*
             * String query =
             * "SELECT * FROM Lider WHERE Documento_Identidad  = \""+documento_identidad+
             * "\""; //Creamos objeto para procesar query Statement statement =
             * this.conn.createStatement(); //Ejecutar y obtener datos a partir del query
             * ResultSet resultado = statement.executeQuery(query);
             */

            String query = "SELECT * FROM Lider WHERE Documento_Identidad  = ?";
            // Obtener conexión a la BD
            Connection conn = DatabaseController.conectar_bd();
            // Prepara el query antes de su ejecución
            PreparedStatement pStatement = conn.prepareStatement(query);
            pStatement.setString(1, documento_identidad);

            // Ejecutar la consulta
            ResultSet resultado = pStatement.executeQuery();

            // Acceder y verificar que halla un registro de respuesta
            if (resultado.next()) {
                // Actualizar los atributos con los datos obtenidos de la consulta
                objLider.setId(resultado.getInt("ID_Lider"));
                objLider.setNombre(resultado.getString("Nombre"));
                objLider.setPrimer_apellido(resultado.getString("Primer_Apellido"));
                objLider.setSegundo_apellido(resultado.getString("Segundo_Apellido"));
                objLider.setSalario(resultado.getInt("Salario"));
                objLider.setCargo(resultado.getString("Cargo"));
                objLider.setCiudad_residencia(resultado.getString("Ciudad_Residencia"));
                objLider.setClasificacion(resultado.getInt("Clasificacion"));
                objLider.setDocumento_identidad(resultado.getString("Documento_Identidad"));
                objLider.setFecha_nacimiento(resultado.getString("Fecha_Nacimiento"));
            }
            // Cerrar conexión
            conn.close();
            pStatement.close();
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
        }

        return objLider;
    }

    public void insertar_lider(Lider objLider) {
        // Estructura del query
        String query = "INSERT INTO Lider(ID_Lider, Nombre, Primer_Apellido, Segundo_Apellido, Salario, Ciudad_Residencia, Cargo, Clasificacion, Documento_Identidad, Fecha_Nacimiento) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        try {
            Connection conn = DatabaseController.conectar_bd();
            PreparedStatement pStatement = conn.prepareStatement(query);
            pStatement.setInt(1, objLider.getId());
            pStatement.setString(2, objLider.getNombre());
            pStatement.setString(3, objLider.getPrimer_apellido());
            pStatement.setString(4, objLider.getSegundo_apellido());
            pStatement.setInt(5, objLider.getSalario());
            pStatement.setString(6, objLider.getCiudad_residencia());
            pStatement.setString(7, objLider.getCargo());
            pStatement.setInt(8, objLider.getClasificacion());
            pStatement.setString(9, objLider.getDocumento_identidad());
            pStatement.setString(10, objLider.getFecha_nacimiento());

            if (pStatement.executeUpdate() == 1) {
                System.out.println("¡Lider registrado con éxito!");
            }
            //Cerrar la conexión
            conn.close();
            pStatement.close();
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
            System.err.println(e.getMessage());
        }
    }

    public void actualizar_lider(Lider lider, int id){

        String query = "UPDATE Lider SET ID_Lider = ?, Nombre = ?, Primer_Apellido = ?, Segundo_Apellido = ?, Salario = ?, Cargo = ?, Ciudad_Residencia = ?, Clasificacion = ?, Documento_Identidad = ?, Fecha_Nacimiento = ? WHERE ID_Lider = ?";

        try {
            Connection conn = DatabaseController.conectar_bd();
            PreparedStatement pStatement = conn.prepareStatement(query);
            pStatement.setInt(1, lider.getId());
            pStatement.setString(2, lider.getNombre());
            pStatement.setString(3, lider.getPrimer_apellido());
            pStatement.setString(4, lider.getSegundo_apellido());
            pStatement.setInt(5, lider.getSalario());
            pStatement.setString(6, lider.getCargo());
            pStatement.setString(7, lider.getCiudad_residencia());
            pStatement.setInt(8, lider.getClasificacion());
            pStatement.setString(9, lider.getDocumento_identidad());
            pStatement.setString(10, lider.getFecha_nacimiento());
            pStatement.setInt(11, id);

            if( pStatement.executeUpdate() == 1){
                System.out.println("Información actualizada");
            }
            conn.close();
            pStatement.close();
        } catch (Exception e) {
            //TODO: handle exception
            System.out.println(e);
        }
     }

}
